
include /usr/share/dpkg/architecture.mk

NAME = hidl-gen
SOURCES = main.cpp
SOURCES_libhidl-gen = \
  Annotation.cpp \
  ArrayType.cpp \
  CompoundType.cpp \
  ConstantExpression.cpp \
  DeathRecipientType.cpp \
  DocComment.cpp \
  EnumType.cpp \
  HandleType.cpp \
  HidlTypeAssertion.cpp \
  Interface.cpp \
  Location.cpp \
  MemoryType.cpp \
  Method.cpp \
  NamedType.cpp \
  PointerType.cpp \
  FmqType.cpp \
  RefType.cpp \
  ScalarType.cpp \
  Scope.cpp \
  StringType.cpp \
  Type.cpp \
  TypeDef.cpp \
  VectorType.cpp
SOURCES_libhidl-gen-ast = \
  Coordinator.cpp \
  generateCpp.cpp \
  generateCppAdapter.cpp \
  generateCppImpl.cpp \
  generateDependencies.cpp \
  generateJava.cpp \
  generateVts.cpp \
  AST.cpp
SOURCES_libhidl-gen-ast += $(OUT_DIR)/hidl-gen_l.cpp hidl-gen_y.cpp
SOURCES_libhidl-gen-hash = hashing/Hash.cpp
SOURCES_libhidl-gen-utils = \
  FQName.cpp \
  FqInstance.cpp
SOURCES_libhidl-gen-host-utils = \
  StringHelper.cpp \
  Formatter.cpp
SOURCES_libhidl-gen-utils := $(foreach source, $(SOURCES_libhidl-gen-utils), utils/$(source))
SOURCES_libhidl-gen-host-utils := $(foreach source, $(SOURCES_libhidl-gen-host-utils), host_utils/$(source))

CXXFLAGS += -std=gnu++17 -D_FILE_OFFSET_BITS=64

CPPFLAGS += \
  -D__ANDROID_DEBUGGABLE__ \
  -D__STDC_CONSTANT_MACROS \
  -D__STDC_FORMAT_MACROS \
  -DANDROID \
  -DANDROID_STRICT \
  -D_LIBCPP_ENABLE_THREAD_SAFETY_ANNOTATIONS \
  -I. \
  -Iinclude_hash \
  -Iinclude_hash/hidl-hash \
  -Ihost_utils \
  -Ihost_utils/include \
  -Ihost_utils/include/hidl-util \
  -Ihashing \
  -Ihashing/include \
  -Iutils \
  -Iutils/include \
  -Iutils/include/hidl-util \

LDFLAGS += -Wl,-rpath=/usr/lib/$(DEB_HOST_MULTIARCH)/android \
           -L/usr/lib/$(DEB_HOST_MULTIARCH)/android -lbase -lcrypto -llog -lssl

ifeq ($(DEB_HOST_ARCH), armel)
  # -latomic should be the last library specified
  # https://github.com/android/ndk/issues/589
  LDFLAGS += -latomic
endif

build: $(SOURCES) $(SOURCES_libhidl-gen) $(SOURCES_libhidl-gen-ast) $(SOURCES_libhidl-gen-hash) $(SOURCES_libhidl-gen-utils) $(SOURCES_libhidl-gen-host-utils)
	mkdir --parents $(OUT_DIR)
	$(CXX) $^ -o $(OUT_DIR)/$(NAME) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS)

$(OUT_DIR)/hidl-gen_l.cpp: hidl-gen_l.ll hidl-gen_y.cpp
	mkdir --parents $(OUT_DIR)
	flex -o $@ $<
